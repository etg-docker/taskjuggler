#!/bin/bash

source ../settings.sh

echo "Build image $IMAGENAME"
echo

docker build \
	--progress=plain \
	--build-arg TASKJUGGLER_VERSION=$TASKJUGGLER_VERSION \
	--tag $IMAGENAME:$DOCKER_TAG_CURRENT \
	.

docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_LATEST
docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_INTERNAL
