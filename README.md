# etg-docker: ekleinod/taskjuggler

## Quick reference

- **Maintained by:** [ekleinod](https://gitlab.com/etg-docker/taskjuggler/)
- **Docker hub:** [ekleinod/taskjuggler](https://hub.docker.com/r/ekleinod/taskjuggler)
- **Where to get help:** [gitlab](https://gitlab.com/etg-docker/taskjuggler/), [issue tracker](https://gitlab.com/etg-docker/taskjuggler/-/issues)


## Supported tags

- `2.0.0`, `2.0.0-3.8.1`, `latest`
- `1.1.0`, `1.1.0-3.7.2`
- `1.0.0`, `1.0.0-3.7.1`


## What are these images?

This is a docker image for [taskjuggler](https://taskjuggler.org) ([github](https://github.com/taskjuggler/TaskJuggler)).
The image has the name `ekleinod/taskjuggler`.

For the complete changelog, see [changelog.md](https://gitlab.com/etg-docker/taskjuggler/-/blob/main/changelog.md)

The image is based on the [docker image tj3](https://hub.docker.com/r/treibholz/tj3) by [treibholz](https://github.com/treibholz/docker-taskjuggler) which unfortunately was not updated for a while.

It uses the Ruby Gem [Taskjuggler](https://rubygems.org/gems/taskjuggler).

Additionally installed programs:

- [WEBrick HTTP server](https://github.com/ruby/webrick)
	- test the server (and see an example call) with [test/test_server.sh](https://gitlab.com/etg-docker/taskjuggler/-/blob/main/test/test_server.sh)
	- stop the test with `ctrl+c`


## How to use the image

You can use the image as follows:

~~~ shell
docker run \
	--rm=true \
	--name <containername> \
	--net="none" \
	--volume "${PWD}/":/tj3 \
	--user `id -u` \
	ekleinod/taskjuggler \
		--output-dir <directory> \
		<projectfile>
~~~

Please note, that `/tj3` is the work directory of the image.

For a simple test showing the taskjuggler help page, run

~~~ bash
$ docker run --rm --name taskjuggler ekleinod/taskjuggler
~~~

You can see examples at work in the test folder.
Clone the repository or download the folder.
Call them as follows:

~~~ bash
$ cd test
$ ./test_help.sh
$ ./test_hello-world.sh
$ ./test_version.sh
$ ./test_server.sh
~~~

Stop the server test with `ctrl+c` as stated in the terminal output.


## Releases

The latest release will always be available with:

- `ekleinod/taskjuggler:latest`

There are two naming schemes:

1. `ekleinod/taskjuggler:<internal>-<taskjuggler>`

	Example: `ekleinod/taskjuggler:1.0.0-3.7.1`

2. internal version number `ekleinod/taskjuggler:<major>.<minor>.<patch>`

	Example: `ekleinod/taskjuggler:1.0.0`


## Build locally

In order to build the image locally, clone the repository and call

~~~ bash
$ cd image
$ ./build_image.sh
~~~

## Git-Repository

The branching model regards to the stable mainline model described in <https://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the main branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.

## Copyright

Copyright 2024-2025 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
