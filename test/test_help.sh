source ../settings.sh

echo "Empty call - shows help."
echo

docker run \
	--rm \
	--user `id -u` \
	--name $CONTAINERNAME \
	$IMAGENAME
