source ../settings.sh

echo "Building tutorial project."
echo

mkdir --parent example/generated

docker run \
	--rm=true \
	--name $CONTAINERNAME \
	--net="none" \
	--volume "${PWD}/":/tj3 \
	--user `id -u` \
	$IMAGENAME \
		--output-dir "example/generated" \
		"example/tutorial.tjp"
