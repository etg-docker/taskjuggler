source ../settings.sh

echo "Starts the server with tutorial project."
echo
echo "Call the following pages:"
echo "http://localhost:8080/ - welcome page of the server"
echo "http://localhost:8080/taskjuggler - project page, tutorial 'Accounting Software' should appear"
echo "http://localhost:8080/taskjuggler?project=acso;report=frame.index - project overview"
echo
echo "If everything works, stop the server with ctrl+c"
echo

mkdir --parent example/testserver
cd example/testserver
cp ../taskjuggler.rc .
cp ../tutorial.tjp .

docker run \
	--rm=true \
	--name $CONTAINERNAME \
	--volume "${PWD}/":/tj3 \
	--user `id -u` \
	--entrypoint tj3d \
	--publish 8080:8080 \
	$IMAGENAME \
		--dont-daemonize \
		--webserver \
		"tutorial.tjp"
