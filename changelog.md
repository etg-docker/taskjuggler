# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].


## [Unreleased]


## [2.0.0-3.8.1] - 2025-01-20

### Added

- webrick #3

### Changed

- taskjuggler `3.8.1`


## [1.1.0-3.7.2] - 2024-03-01

### Changed

- taskjuggler `3.7.2`
- always use latest alpine #2


## [1.0.0-3.7.1] - 2024-03-01

- initial version

### Added

- taskjuggler `3.7.1`


[Unreleased]: https://gitlab.com/etg-docker/taskjuggler/-/compare/2.0.0-3.8.1...HEAD
[2.0.0-3.8.1]: https://gitlab.com/etg-docker/taskjuggler/-/compare/1.1.0-3.7.2...2.0.0-3.8.1
[1.1.0-3.7.2]: https://gitlab.com/etg-docker/taskjuggler/-/compare/1.0.0-3.7.1...1.1.0-3.7.2
[1.0.0-3.7.1]: https://gitlab.com/etg-docker/taskjuggler/-/tags/1.0.0-3.7.1

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
